# Falling In Abyss

製作分工 林辰-主視覺 美術設計 魏均佾-美術及影片製作 紀棋峰-關卡設計及程式撰寫 周祐羽-PPT製作及美術設計

# 遊戲主視覺 MADE BY 林辰

![遊戲主視覺圖](https://gitlab.com/h5921512/falling-in-abyss/-/raw/main/introduce/%E6%8A%95%E5%BD%B1%E7%89%872.PNG?inline=false)

# 製作動機

![製作動機](https://gitlab.com/h5921512/falling-in-abyss/-/raw/main/introduce/%E6%8A%95%E5%BD%B1%E7%89%874.PNG?inline=false)
![製作動機](https://gitlab.com/h5921512/falling-in-abyss/-/raw/main/introduce/%E6%8A%95%E5%BD%B1%E7%89%879.PNG?inline=false)


# 使用技術

使用Unity及Arduino搭配設計此遊戲

如果有壓力感測器的話，接上電腦後可以讓角色發出子彈，以及擁抱受傷的兒子

遊戲主要玩法，方向鍵移動，Z鍵觸發對話或事件，透過踩踏的方式擊退怪物獲得回憶，Q鍵可以退出回憶
