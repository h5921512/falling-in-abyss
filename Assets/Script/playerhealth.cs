﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class playerhealth : MonoBehaviour
{
    
    public int health;
    public GameObject LoveUI;
    public int LovePoint = 0;
    // Start is called before the first frame update


    // Update is called once per frame
    void Update()
    {
        if (gameObject.transform.position.y < 13)
        {
            Die();
            LovePoint = 0;
        }
        LoveUI.gameObject.GetComponent<Text>().text = ("X "+LovePoint);
   

        void Die()
        {
            SceneManager.LoadScene("FirstPlace");
            
        }
    }
    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "gametool")
        {
            LovePoint++;
            Destroy(col.gameObject);
        }
    }
}
