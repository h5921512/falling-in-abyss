﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class dadcontroller : MonoBehaviour
{
    public float movespeed = 3.0f;
    public int playerjump = 1250;
    public float moveX;
    public bool isground;
    public GameObject book;
    public GameObject chair;
    public GameObject food;
    public GameObject keybroad;
    public GameObject card;
    public int dietimesw=0;
    public Button bookbtn;
    public Button chairbtn;
    public Button foodbtn;
    public Button keybroadbtn;
    public Button cardbtn;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
        playermove();
        PlayerRaycast();
    }
    void playermove()
    {
        moveX = Input.GetAxis("Horizontal");

        if (Input.GetButtonDown("Jump")&&isground==true)
        {
            jump();
        }
        if (moveX != 0)
        {
            GetComponent<Animator>().SetBool("IsRunning", true);
        }
        else
        {
            GetComponent<Animator>().SetBool("IsRunning", false);
        }
        if (moveX > 0.0f )
        {
            GetComponent<SpriteRenderer>().flipX = false;
        }
        else if (moveX < 0.0f )
        {
            GetComponent<SpriteRenderer>().flipX = true;
        }
        gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(moveX * movespeed, gameObject.GetComponent<Rigidbody2D>().velocity.y);
    }
    void jump()
    {     
        GetComponent<Rigidbody2D>().AddForce(Vector2.up * playerjump);
        isground = false;
    }
    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.tag == "ground")
        {
            isground = true;
        }
    }
    void PlayerRaycast()
    {
        RaycastHit2D hit = Physics2D.Raycast(transform.position, Vector2.down);
        if (hit.collider!=null&&hit.distance < 1.5f && hit.collider.tag == "enemy")
        {
            GetComponent<Rigidbody2D>().AddForce(Vector2.up * 1500);
            hit.collider.gameObject.GetComponent<Rigidbody2D>().AddForce(Vector2.right * 500);
            hit.collider.gameObject.GetComponent<BoxCollider2D>().enabled = false;
            hit.collider.gameObject.GetComponent<enemymove>().enabled = false;
            Destroy(hit.collider.gameObject);
            if (dietimesw == 0)
            {
                
                Instantiate(book, new Vector2(transform.position.x, 17), Quaternion.identity);
                bookbtn.GetComponent<Button>().interactable = true;
                dietimesw++;
            }
            else if (dietimesw == 1)
            {
                Instantiate(chair, new Vector2(transform.position.x, 21), Quaternion.identity);
                chairbtn.GetComponent<Button>().interactable = true;
                dietimesw++;
            }
            else if (dietimesw == 2)
            {
                Instantiate(food, new Vector2(transform.position.x, 17), Quaternion.identity);
                foodbtn.GetComponent<Button>().interactable = true;
                dietimesw++;
            }
            else if (dietimesw == 3)
            {
                Instantiate(card, new Vector2(transform.position.x, 20), Quaternion.identity);
                cardbtn.GetComponent<Button>().interactable = true; 
                dietimesw++;
            }
            else if (dietimesw == 4)
            {
                Instantiate(keybroad, new Vector2(transform.position.x, 18), Quaternion.identity);
                keybroadbtn.GetComponent<Button>().interactable = true;
                dietimesw++;
            }
        }
    }
}
