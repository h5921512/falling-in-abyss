﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class enemymove : MonoBehaviour
{
    public int EnemySpeed;
    public int XMoveDirection=1;
    public float walking = 0.5f;
    public float cooldown;
    
    // Update is called once per frame
    void Update()
    {
        RaycastHit2D hit = Physics2D.Raycast(transform.position, new Vector2(XMoveDirection, 0));
        Vector2 enemy = transform.localScale;
        enemy.x = -XMoveDirection;
        transform.localScale = enemy;
        gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(XMoveDirection, 0) * EnemySpeed;
        if (hit.distance < 0.7f)
        {
            if (hit.collider!=null&&hit.collider.tag == "Player")
            {
                Destroy(hit.collider.gameObject);
                SceneManager.LoadScene("FirstPlace");
            }
        }
        if (Time.time>=cooldown)
        {
            flip();
            
        }

    }
    void flip()
    {
        if (XMoveDirection > 0)
        {
            cooldown = Time.time + walking;
            XMoveDirection = -1;
            
        }
        else
        {
            cooldown = Time.time + walking;
            XMoveDirection = 1;
        }
    }
    }
