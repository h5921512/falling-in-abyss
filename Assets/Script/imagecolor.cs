﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class imagecolor : MonoBehaviour
{
    public Image image1;
    public Image image2;
    public Image image3;
    public Image image4;
    public Image image5;
    void Start()
    {
        image1.GetComponent<Image>().enabled = false;
        image2.GetComponent<Image>().enabled = false;
        image3.GetComponent<Image>().enabled = false;
        image4.GetComponent<Image>().enabled = false;
        image5.GetComponent<Image>().enabled = false;
    }
     void Update()
    {
        if (Input.GetKeyDown(KeyCode.Q))
        {
            image1.GetComponent<Image>().enabled = false;
            image2.GetComponent<Image>().enabled = false;
            image3.GetComponent<Image>().enabled = false;
            image4.GetComponent<Image>().enabled = false;
            image5.GetComponent<Image>().enabled = false;
        }
    }
    public void first()
    {
        image1.GetComponent<Image>().enabled = true;
        image1.GetComponent<Image>().color = new Color(255, 255, 255, 255);
    }
    public void second()
    {
        image2.GetComponent<Image>().enabled = true;
        image2.GetComponent<Image>().color = new Color(255, 255, 255, 255);
    }
    public void third()
    {
        image3.GetComponent<Image>().enabled = true;
        image3.GetComponent<Image>().color = new Color(255, 255, 255, 255);
    }
    public void fourth()
    {
        image4.GetComponent<Image>().enabled = true;
        image4.GetComponent<Image>().color = new Color(255, 255, 255, 255);
    }
    public void fifth()
    {
        image5.GetComponent<Image>().enabled = true;
        image5.GetComponent<Image>().color = new Color(255, 255, 255, 255);
    }
}
