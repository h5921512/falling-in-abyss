﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class NewBehaviourScript : MonoBehaviour
{
    public GameObject book;
    public GameObject chair;
    public GameObject food;
    public GameObject keybroad;
    public GameObject card;
    public int dietimes = 0;
    public Button bookbtn;
    public Button chairbtn;
    public Button foodbtn;
    public Button keybroadbtn;
    public Button cardbtn;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "bullet")
        {
            Destroy(gameObject);
            if (dietimes == 0)
            {
                Instantiate(book, new Vector2(transform.position.x, 17), Quaternion.identity);
                bookbtn.GetComponent<Button>().interactable = true;
                dietimes++;
            }
            else if (dietimes == 1)
            {
                Instantiate(chair, new Vector2(transform.position.x, 21), Quaternion.identity);
                chairbtn.GetComponent<Button>().interactable = true;
                dietimes++;
            }
            else if (dietimes == 2)
            {
                Instantiate(food, new Vector2(transform.position.x, 17), Quaternion.identity);
                foodbtn.GetComponent<Button>().interactable = true;
                dietimes++;
            }
            else if (dietimes == 3)
            {
                Instantiate(card, new Vector2(transform.position.x, 20), Quaternion.identity);
                cardbtn.GetComponent<Button>().interactable = true;
                dietimes++;
            }
            else if (dietimes == 4)
            {
                Instantiate(keybroad, new Vector2(transform.position.x, 18), Quaternion.identity);
                keybroadbtn.GetComponent<Button>().interactable = true;
                dietimes++;
            }
        }
        
    }
}
