﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class aicontroller : MonoBehaviour
{
    public float movespeed = 3.0f;
    public int playerjump = 1250;
    public float moveX;
    // Start is called before the first frame update
    void Start()
    {
       
    }

    // Update is called once per frame
    void Update()
    {
        playermove();
    }
    void playermove()
    {
        moveX = Input.GetAxis("Horizontal");

        if (moveX > 0.0f)
        {
            GetComponent<SpriteRenderer>().flipX = false;
        }
        else if (moveX < 0.0f)
        {
            GetComponent<SpriteRenderer>().flipX = true;
        }
        gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(moveX * movespeed, gameObject.GetComponent<Rigidbody2D>().velocity.y);
    }

}
