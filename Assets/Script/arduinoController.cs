﻿using UnityEngine;
using System.Collections;
using System.IO.Ports;
using System;

public class arduinoController : MonoBehaviour
{
    public SerialPort sp = new SerialPort("COM4", 9600);
    bool creatbullet = false;
    // Start is called before the first frame update
    void Start()
    {
        sp.Open();
        sp.ReadTimeout = 1;
        
    }
    public Rigidbody2D bulletPrefab;
    float attackRate = 0.5f;
    float cooldown;
    // Update is called once per frame
    void Update()
    {
        try
        {
            if (Time.time >= cooldown)
            {
                if (sp.IsOpen)
                {
                    string attack = sp.ReadLine();
                    if (attack == "1")
                    {
                        BulletAttack();
                        creatbullet = true;
                    }
                    if (attack == "press")
                    {
                        GetComponent<Animator>().SetBool("IsHug", true);
                    }


                }
            }
            if (Input.GetKeyDown(KeyCode.W))
            {
                GetComponent<Animator>().SetBool("IsHug", true);
            }
            if (Time.time >= cooldown&&creatbullet==true)
            {
                Destroy(GameObject.FindWithTag("bullet"));
                creatbullet = false;
            }
            void BulletAttack()
            {
                Rigidbody2D bPrefab = Instantiate(bulletPrefab, transform.position, Quaternion.identity) as Rigidbody2D;
                bPrefab.GetComponent<Rigidbody2D>().AddForce(Vector2.right * 500);
                cooldown = Time.time + attackRate;
            }
        }
        catch (TimeoutException)
        {

        }


    }
}
   
